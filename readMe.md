# Scandiweb Test  (Product Add/Review App) PHP - OOP

## Description:

This is a **Scandiweb Trainee Junior Developer Test Task** assignment that is done with certian instructions

### Instructions:

> website is php MVC oop focused

> Contain Two Pages [index.php | add-product.php]

### Technologies Used

- HTML + CSS
- Vanilla JavaScript
- PHP native + OOP + MVC Pattern
- Connect Database using PDO Prepare stmt
- Using **fetch with promise** to fetch data between client and server Sides
- lamp image:https://www.flaticon.com/free-icon/lamp_1179267

### Author's Info:

**Name:** Michael Adel
**Email:** michaeladel1992@gmail.com
**Phone:** (+20) 01286683642