<?php
namespace App\Controllers;

use Core\View;
use Core\Message;
use Core\Controller;
use Core\Session;
use App\Models\Product;
use App\Models\Specification;

class Products extends Controller{
  /**
   *@return view products page with all Products data 
   */ 
  function indexAction(){
    $product = new Product;
    $data = $product->displayProduct();
    View::render('Product/index.php',$data);
  }

  /**
   *@return view add product form page with specifications data 
   */
  function addAction(){
    $specification = new Specification;
    $data = $specification->typeArray();
    View::render('Product/add-product.php',$data);
  }

  function addProductAction(){
    new Session; // set sesion 
    if( 
      $_SERVER['REQUEST_METHOD'] == "POST" 
      && 
      isset($_POST['form'])
      &&
      isset($_SESSION['token'])
      ){
        //Add Product Process
        if($_POST['form'] === 'add-product'){
           
            // check Token
            if($_SESSION['token'] === $_POST['token']){
              
              // initiate
              $product  = new Product;
              $product->processingPostProduct($_POST);
     
            
            }else{
              Message::setMsg("Token Expired!");
              Message::$notifications[] = ["other" => Message::getMsg()];
              exit(json_encode(Message::$notifications));
           
            }
        }else{
          self::redirect('./');
        }
    
      }else{
        self::redirect('./');
      }
  }

  function massDeleteAction(){
    if( 
      $_SERVER['REQUEST_METHOD'] == "POST" 
      && 
      isset($_POST['form'])
      ){

        if(
          $_POST['form'] == 'mass-delete-product'
          &&
          isset($_POST['selectedList'])
          ){
            $product = new Product;
            $product->processingMassDelete($_POST);     
          }else{
            self::redirect('./');
          }
      }else{
        self::redirect('./');
      }
 
  }

  protected function before(){
    // echo "(before)";
  }

  protected function after(){
    // echo "(after)";
  }
}