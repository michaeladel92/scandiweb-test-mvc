<?php
namespace App\Models;
use PDO;
use Core\DatabaseConnect;
use Core\Validation;
use Core\Message;

class Specification extends DatabaseConnect{
    /**
     * NOTE::
     * $table must follow lowercase pattern && same as db table names
     * properties name follow as db table names
     * */  
    private 
            $table = ['size','weight','width','height','length'],
            $size, $weight, $width, $height, $length, $type_id,$keys = "`type_id`,",$vals = [];
   
       
    
    function typeArray(){
     return $this->query(" * " , "types");
    }
    /*
     * Filter & Validate Specifications
     * @set Message Notifications
     * @return True | False
     */ 
    
    function validateProductType($productType,array $typesArry = []){
      $validation = new Validation;
      //product type empty
      if(!$validation->validate($productType,'empty')){
      
        Message::setMsg("Product Type is required!");
        Message::$notifications[] = ["productType" => Message::getMsg()];
        return false;
      
      }
      
      //FixBig: need to make lowercase in array_keys
      $productType = $validation->filter($productType);

      foreach($this->typeArray() as $key => $val){
        
        $type[$val['id']] = strtolower($val['type']);
      
      }
      
      if(!$validation->validate($validation->filter($productType),'inArray',null,null,$type)){
      
        Message::setMsg("Oops, an error accured, \"{$productType}\" not found in our database, please try again!");
        Message::$notifications[] = ["other" => Message::getMsg()];
      
      }else{

          $this->type_id = array_keys($type, $productType)[0];// set id
           
          $table =  $this->table;

          
          for($i=0; $i <= count($table) -1; $i++ ){
             //check if the given array exist in our $table array
            if(array_key_exists($table[$i], $typesArry)){
              //make sure that the given property is exist
              if(property_exists('App\Models\Specification',$table[$i])){
                //set the property to the given value | filter the value
               $this->{$table[$i]} =  $validation->filter($typesArry[$table[$i]],'dot');
               
              }  
            }
          }
         //validate the properties that set
         foreach($this->table as $property){
          
          if(isset($this->{$property})){
            $this->keys .= "`{$property}`,";
            $this->vals[] = $this->{$property};
            //empty    
            if(!$validation->validate($this->{$property},'empty')){
              
              Message::setMsg("{$property} field is required!");
              Message::$notifications[] = ["{$property}" => Message::getMsg()];
              
            }
            //numeric
            elseif(!$validation->validate($this->{$property},'numeric')){
            
              Message::setMsg("{$property} must be  numeric");
              Message::$notifications[] = ["{$property}" => Message::getMsg()];
              
            }
            //first char cannot be zero
            elseif(!$validation->validate($this->{$property},'firstChar')){

              Message::setMsg("{$property} cannot start with zero");
              Message::$notifications[] = ["{$property}" => Message::getMsg()];

            }
            // max 10 char
            elseif(!$validation->validate($this->{$property},'max',1,11)){
              
              Message::setMsg("{$property} max 10 char");
              Message::$notifications[] = ["{$property}" => Message::getMsg()];

            } 
          }
        }
          
          if(count(Message::$notifications) > 0){return false;}
          else{return true;}
      }
    
    }
    /*
     * INSERT To DataBase Process:
     * @RETURN new Row Id
     */
    function insertSpecificationProcess(){
      
      array_unshift($this->vals,$this->type_id);
      $this->insert('specifications',$this->keys, $this->vals);
      if($this->execute()){
        
        return $this->lastId();
      
      }else{ return false;}
    }
  
    function __destruct(){
      // echo "end Specification";
      $this->size = null;
      $this->weight = null;
      $this->width = null;
      $this->height = null;
      $this->length = null;
      $this->type_id = null;
      $this->keys = "`type_id`,";
      $this->vals = [];
     
    }

}