<?php
namespace App\Models;
use PDO;
use Core\DatabaseConnect;
use Core\Validation;
use Core\Message;
use App\Models\Specification;

class Product extends DatabaseConnect{

  private 
  $table = ['sku','name','price','specifications_id'], 
  $sku, $name, $price, $specifications_id,$key,$val = [];

  /**
   * Filter & Validate fields:
   * sku | name | price
   * @set Message Notifications
   * @return True,False
   */
  function validateProducts($sku,$name,$price){
      $validate = new Validation;
      // set & filter
      $this->sku = $validate->filter($sku);
      $this->name = $validate->filter($name);
      $this->price = $validate->filter($price,'coma');

      $targetSku = false; 
      $targetName = false; 
      $targetPrice = false; 

      // sku validate
      if(!$validate->validate($this->sku,'empty')){
      
          Message::setMsg("sku field is required");
          Message::$notifications[] = ["sku" => Message::getMsg()];

      }
      elseif(!$validate->validate($this->sku,'string')){
      
          Message::setMsg("accepted character only (Aa to Zz / 0-9 [#!@*_-+$])");
          Message::$notifications[] = ["sku" => Message::getMsg()];

      }
      elseif(!$validate->validate($this->sku,'max')){

          Message::setMsg("maximum character accepted is 50");
          Message::$notifications[] = ["sku" => Message::getMsg()];

      }
      elseif($this->queryCount("*" , 'products' , "WHERE `sku` = '{$this->sku}'") > 0){
          
          Message::setMsg("sku:\"{$this->sku}\" already exist in ourdatabase!");
          Message::$notifications[] = ["sku" => Message::getMsg()];

      }else{ $targetSku = true; }

      // name validate
      if(!$validate->validate($this->name,'empty')){
      
          Message::setMsg("name field is required");
          Message::$notifications[] = ["name" => Message::getMsg()];
      
      }
      elseif(!$validate->validate($this->name,'string')){
      
          Message::setMsg("accepted character only (Aa to Zz / 0-9 [#!@&*_-+$])");
          Message::$notifications[] = ["name" => Message::getMsg()];
      
      }
      elseif(!$validate->validate($this->name,'max')){

          Message::setMsg("maximum character accepted is 50");
          Message::$notifications[] = ["name" => Message::getMsg()];

      }else{ $targetName = true; }

      // price validate
      if(!$validate->validate($this->price,'empty')){
      
          Message::setMsg("price field is required");
          Message::$notifications[] = ["price" => Message::getMsg()];

      }
      elseif(!$validate->validate($this->price,'firstChar')){
      
          Message::setMsg("price cannot start with number zero");
          Message::$notifications[] = ["price" => Message::getMsg()];

      }
      elseif(!$validate->validate($this->price,'numeric')){
      
          Message::setMsg("accepted characters only (0-9)");
          Message::$notifications[] = ["price" => Message::getMsg()];
      
      }
      elseif(!$validate->validate($this->price,'max',null,10)){

          Message::setMsg("maximum price accepted is 999,999,999 $");
          Message::$notifications[] = ["price" => Message::getMsg()];
      
      }else{ $targetPrice = true; }
      
      if($targetSku && $targetName && $targetPrice){return true;}
      else{return false;}
      
  }

  /**
   * PARAM of 2 DB Tables [specifications | products]
   * typesArry Accept Key ['size','weight','width','height','length'] - ONLY
   * 
   */
  function insertProduct($sku,$name,$price,$productType,array $typesArry = []){
        $specification = new Specification;
      // result will return false if validate not passed
      $specificationResult = $specification->validateProductType($productType,$typesArry);
      $validateResult = $this->validateProducts($sku,$name,$price);

      if($specificationResult && $validateResult){
      
          // Transaction Process [Google Searhed]
          try{
              $conn = $this->conn();   
              //We start our transaction.
              $conn->beginTransaction();

              // Query:1 insert Specification in DB
              $this->specifications_id = $specification->insertSpecificationProcess();


              //  Query: 2 insert Product in DB
              foreach($this->table as $property){
              
                  if(property_exists('App\Models\Product',$property)){
                      
                      if(isset($this->{$property})){
                          $this->key .= "`{$property}`,";
                          $this->val[] = $this->{$property};
                      }
                  }
              }
              $this->insert('products',$this->key,$this->val);
              if($this->execute()){
                  // redirect to DB
                  Message::$notifications[] = ["redirect" => 'home'];
              }
          
              //so commit the changes.
              $conn->commit();
          }
          catch(Exception   $e){
              echo  $e->getMessage();
              //Rollback the transaction.
              $conn->rollBack();
          }
          
      
      }        
       
  }

  /**
   * GET the $_POST Values to INSERT in DB
   */ 
  public function processingPostProduct(array $post){

      $sku         = $post['sku'];
      $name        = $post['name'];
      $price       = $post['price'];
      $productType = $post['productType'];

      $specifications = ['size','weight','height','width','length'];
      $selectedSpecifications = [];
      foreach($specifications as $item){

        if(isset($post[$item])){$selectedSpecifications[$item] = $post[$item];}

      }
    
      /**
       * PARAM:sku|name|price|productType|specificationsArray
       */
      $this->insertProduct($sku,$name,$price,$productType,$selectedSpecifications);
      exit(json_encode(Message::$notifications));
  }

  /**
   * Display Product
   * @return array 
   */ 
  function displayProduct(){
      $column = "
      products.*,
      specifications.size,
      specifications.weight,
      specifications.width,
      specifications.height,
      specifications.length,
      specifications.type_id,
      types.type
    ";
      $other =  "
              INNER JOIN 
                      `specifications`
                  ON
                      `specifications`.`id` = `products`.`specifications_id`
              INNER JOIN 
                      `types`
                  ON
                      `specifications`.`type_id` = `types`.`id`
              ORDER BY `products`.`id` DESC         
          "; 
          
      $rows = $this->query($column , "products" , $other);
      //RULE:: need to be lowerCased for compare
      $encodingIds = ['id','type_id','specifications_id'];
      $data = [];
      $types = ['dvd' => ['size'],
                'book' => ['weight'],
                'furniture' => ['width','length','height']
            ];
      $typeKeys = [
                    'size' => 'mb',
                    'weight' => 'kg',
                    'width' => 'W X ',
                    'height' => 'H',
                    'length' => 'L X '
                ];      
      
      foreach ($rows as $products){
      
          foreach($products as $key => $val){
              //encode ids 
              if(in_array(strtolower($key),$encodingIds)){
            
                  $products[$key] = base64_encode($val);
              }
              // money format
              if(strtolower($key) == 'price'){
                  $products[$key] = '$' .number_format($val);
              }
              // format specifications depent on types
               
                $productType = strtolower($products['type']);
                if(array_key_exists($productType, $types)){
                        $name = '';
                        foreach($types[$productType] as $item){
                            $name .= $products[$item] . $typeKeys[$item];
                        }
                        $products['specification_view'] = $name;
                }else{
                    $products['specification_view'] = 'n/a';
                } 

          }
          unset($products['size']);
          unset($products['weight']);
          unset($products['width']);
          unset($products['height']);
          unset($products['length']);

          $data[] = $products;
      }
     
      return $data;

  }

  /**
   * DELETE Multiple Products by Specification Ids
   * Relations between Table Cascade,
   * when Choosing to delete by specification id it will auto delete product as well  
   */
  protected function deleteMassProducts(array $SpecificationIds){
          
      $condition = "WHERE `id` IN(";
      $condition .= implode(',',$SpecificationIds);
      $condition .= ")";
      // check if ids exist
      if($this->queryCount('id' , 'specifications' , $condition) > 0){
          
          $num = count($SpecificationIds);
          $i = 0;
          $questionMarks = '';
          // creating a questionmark string
          while($i < $num){

              if($i !== $num -1){$questionMarks .= "?,";}
              else{$questionMarks .= "?";}
              
              $i++;
          }

          $condition = "`id` IN(";
          $condition .= $questionMarks;
          $condition .= ")";
          // delete process
          $this->delete('specifications', $condition);  
          $this->bindParamQuestions($num,$SpecificationIds);
          if($this->execute()){

              Message::setMsg("deleted successfully!");
              Message::$notifications[] = ["redirect" => Message::getMsg()];
          }
      }
  }

  /**
   *get $_POST values & decode the Ids then Delete from DB
   */ 

  function processingMassDelete(array $post){

      $list = json_decode($post['selectedList']);
      if(count($list) !== 0){
        
        // decoding Specifications id
        $SpecificationsIds = array_map(function($item){
          $validate = new Validation;
          return $validate->filter((int)base64_decode($item));
        },$list);
   
        $this->deleteMassProducts($SpecificationsIds);
        if(count(Message::$notifications) > 0){

          exit(json_encode(Message::$notifications));
        }
        

      }
  }

  function __destruct(){
      
      parent::__destruct();
      // echo "destruct Product";
      $this->name = null;
      $this->price = null;
      $this->specifications_id = null;
      $this->key = null;
      $this->val = [];
  }
}