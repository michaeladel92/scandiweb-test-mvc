<?php

require_once "../autoload.php";



// Router for URL
$router = new Core\Router;

$router->add('',['controller' =>'Products','action' => 'index']);
$router->add('add-product',['controller' =>'Products','action' => 'add']);
$router->add('delete-api',['controller' =>'Products','action' => 'mass-delete']);
$router->add('add-api',['controller' =>'Products','action' => 'add-product']);
$router->add('{controller}/{action}');

$url = $_SERVER['QUERY_STRING'];
$router->dispatch($url);

// echo "<pre>";
// print_r($router->getParams());
