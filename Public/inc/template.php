<?php
  $url =  dirname(dirname(__DIR__));
  require_once "{$url}/autoload.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- SEO -->
  <meta name="description" content="description content here!">
  <meta name="keywords" content="php, mysql, html, css, javaScript,scandiweb test">
  <meta name="robots" content="index, follow" />

  <!-- fontawsome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css"
    integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">
  <!-- Style - CSS -->
  <link rel="stylesheet" href="css/style.css">
  <?php if(Core\Router::matchUrl('')):?>
  <link rel="stylesheet" href="css/product-list.css">
  <?php elseif (Core\Router::matchUrl('add-product')): ?>
  <link rel="stylesheet" href="css/product-add.css">
  <?php endif; ?>

  <title><?php echo Core\Router::$title; ?></title>
</head>

<body>
 
 <?php require_once "inc/nav.php"; ?>

    <!-- main content -->

<?php 
  if(Core\Router::matchUrl(''))
  {
    require_once "inc/index.php";
  }
  elseif (Core\Router::matchUrl('add-product'))
  {
    require_once "inc/add-product.php";
  }
  
?>
    <!-- main content -->

<!-- footer-start -->
  <footer class="footer">
    <h3>scandiweb test assignment</h3>
  </footer>
  <!-- footer-end -->
</body>
<script src="js/app.js"></script>
<?php if(Core\Router::matchUrl('')):?>

<script src="js/product-list.js"></script>

<?php elseif (Core\Router::matchUrl('add-product')): ?>

  <script src="js/product-add.js"></script>

<?php endif; ?>
</html>