<!-- product-list-start -->
<main class="products">
   <div class="heading">
     <h1 class="title upper-case">product list</h1>
     <button id="mass-delete" class="upper-case teko-font danger">mass delete</button>
   </div>
   <div class="container" id="product-list">
   <?php if(count($data) > 0):
            foreach($data as $item):
   ?>
      <article>
        <input type="checkbox" value="<?=$item['specifications_id']?>" class="delete-checkbox">
        <div class="content">
            <ul>    
            <li>sku: <?=$item['sku']?></li>
            <li>name: <?=$item['name']?></li>
            <li><?=$item['price']?></li>
            <li>type: <?=$item['type']?> </li>
            <li><?=$item['specification_view']?> </li>
            </ul>
        </div>
      </article>
    <?php
    endforeach;
    else:
    ?>
      <div class="default-notification">
          <div class="image-container">
          <img src="img/lamp.png" alt="lamp note">
          </div>
          <h3>no products available yet!</h3>
      </div>  
   </div>
   <?php endif; ?>
 </main>
 <!-- product-list-end -->
