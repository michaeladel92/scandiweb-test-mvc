 <!-- product-add-start -->
 <main class="products">
    <div class="heading">
      <h1 class="title upper-case">Add product</h1>
      <a href="./" class="upper-case teko-font danger">cancel</a>
    </div>
    <div class="container">
      <form action="post" id="product_form" class="product_form">
        <input type="hidden" name="token" value="<?=$session->set_token()?>">
        <div class="row">
          <!-- sku -->
          <div class="form-controller">
            <label for="sku" class=" Oswald-font">sku</label>
            <input type="text" id="sku" name="sku" placeholder="product sku">
            <small class="validate"></small>
          </div>
          <!-- name -->
          <div class="form-controller">
            <label for="name" class=" Oswald-font">name</label>
            <input type="text" id="name" name="name" placeholder="product name">
            <small class="validate"></small>
          </div>
          <!-- price -->
          <div class="form-controller">
            <label for="price" class=" Oswald-font">price ($)</label>
            <input type="text" id="price" name="price" placeholder="product price (US Dollar)">
            <small class="validate"></small>
          </div>
          <!-- product type -->
          <div class="form-controller">
            <label for="productType" class=" Oswald-font">product type</label>
            <select name="productType" id="productType">
              <option value="">choose</option>
              <?php    
              foreach($data as $item){
 
                $type = $item['type'];
                echo "<option value='{$type}'>{$type}</option>";
              
              }
              ?>
            </select>
            <small class="validate"></small>
          </div>
            
          <span id="productOption" class="productOption"></span>

          <!-- submit btn -->
          <div class="form-controller">
            <input id="other" type="submit" name="submit" value="save" class="upper-case Oswald-font">
            <small class="validate"></small>
          </div>
        </div>
      </form>
    </div>
  </main>
  <!-- product-add-end -->


