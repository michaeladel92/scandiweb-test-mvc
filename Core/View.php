<?php

namespace Core;

class View
{

    /**
     * Render a view file
     *
     * @param string $view  The view file
     * @param array $args  Associative array of data to display in the view (optional)
     *
     * @return void
     */
    public static function render($view, $data = [])
    {
        extract($data, EXTR_SKIP);

        $file = "../App/Views/$view";  // relative to Core directory

        if (file_exists($file)) {
            require $file;
        } else {
            echo "$file not found";
        }
    }
}
