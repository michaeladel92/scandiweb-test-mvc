<?php
namespace Core;

class Validation {


  function filter($value,$flag = null){

      $target = strip_tags($value);
      $target = stripcslashes($target);
      $target = stripslashes($target);
      $target = trim(preg_replace('/[\/]+/', '', $target));
      $target = htmlspecialchars($target);
      $target = strtolower($target);
      $target = trim($target);
      $target = $this->removeExtraSpace($target);
      
      switch($flag){
        case 'coma':
          $target = $this->removeComa($target);
          break;
        case 'dot':
          $target =$this->validDigitsWithDicemal($target);
          break;
        default:
          return $target; 
      }
      return $target;

  }

  function validate($value,$flag,$min = 1,$max = 50,array $arr = []){

    $target = $value;

    switch ($flag) {

      case 'numeric':
        if (!is_numeric($target)) {$target = false;}
        break;
      case 'empty':
        if (empty($target) || $target == '') {$target = false;}
        break;
      case 'max':
        if (strlen($target) >= $max) {$target = false;}
        break;
      case 'min':
        if (strlen($target) <= $min) {$target = false;}
        break;
      case 'firstChar':
        if ($target[0] == 0) {$target = false;}
        break;
      case 'inArray':
        if (!in_array($target, $arr)) {$target = false;}
        break;
      case 'string':
        if(preg_match('/[^A-Za-z0-9\-\s\s+#!@*_$]/', $target)){$target = false;}
        break;
      default:
        $target = false; 

    }

    return $target;

  }

  /**
   * @return oneSpaceEachWord 
   */ 
  protected function removeExtraSpace($value){
      $name = trim(preg_replace('/\s\s+/', ' ', $value));
      return $name;
  }

  /**
   * @return NumbersWithNoComas
   */
  protected function removeComa($value){
      $name = trim(preg_replace('/,/', '', $value));
      $name = trim(preg_replace('/[^\d]+/', '', $name));
      return $name;
  }

  /**
   * @return NumbersWithOneDotsIfAvailable
   */
  protected function validDigitsWithDicemal($value) {
      $name = trim(preg_replace('/[^\d.]+/', '', $value));
      
      if(substr_count($name, '.') > 1){
        // rmv all dots and leave one only [Google Searched]
        $name = preg_replace('/\.(?=.*\.)/', '', $name);
        // if dots in last/first char will be removed
        if(strlen($name) > 1){
          if($name[0] == '.'){$name = substr($name,1,strlen($name));}
          if($name[-1] == '.'){$name = substr($name,0,strlen($name) -1 );}
        }
      }
      
      return $name;
  }
}