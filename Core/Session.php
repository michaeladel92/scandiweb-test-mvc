<?php

namespace Core;

class Session {


  function __construct(){
    ob_start();
    session_start();

  }

  // Generate auto token [Google Searched]
  private function token_generator(){
    $token = openssl_random_pseudo_bytes(16);
    $token = bin2hex($token);
    return $token;
  }

  function set_token(){
    $_SESSION['token'] = $this->token_generator();
    return $_SESSION['token'];
  }
  
  function __destruct(){
    ob_end_flush();
  }
}