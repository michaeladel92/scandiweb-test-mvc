<?php
namespace Core;

class Message{
  public static $message;
  public static $notifications = [];

  public static function setMsg($msg){
    self::$message =  $msg;
  }

  public static function getMsg(){
    return self::$message;
  }
}