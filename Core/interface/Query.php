<?php
namespace Core\interface;

interface Query{

   function conn();
   function query($column , $table , $other = null);
   function queryCount($column , $table , $other = null);
   function insert($table,$column,array $valueArr);
   function delete($table, $other);
   function execute();
   function rowCount();
   function fetch();
   function lastId();
   function bindParam(array $arrKey,array $arrVal);
   function bindParamQuestions(int $num,array $arrVal);

}