<?php
namespace Core;

use PDO;
use Core\interface\Query;
use App\Config;

class DatabaseConnect implements Query {
  
  protected $dbhost, $dbuser, $dbpass, $dbname, $error, $stmt, $dbh, $conn;
  
  public function __construct(){
    // echo "start database";
    $this->dbhost = Config::DB_HOST;
    $this->dbuser = Config::DB_USER;
    $this->dbpass = Config::DB_PASS;
    $this->dbname = Config::DB_NAME;
    $this->conn();
  }

  public function conn(){
  
    try{
      $this->conn = new PDO("mysql:host=".$this->dbhost.";dbname=".$this->dbname."",$this->dbuser,$this->dbpass, array(
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_EMULATE_PREPARES => false
    ));

      $this->dbh = $this->conn;
    
    } catch(PDOException $e) {
    
      die($this->error = "error". $e->getMessage()) ;
    
    }
    
    return $this->dbh;
  
  }

  /**
   * @return array
   */
  public function query($column , $table , $other = null) {

        $sql = "SELECT {$column} FROM `{$table}` {$other}";
        $this->stmt = $this->dbh->query($sql);
        $this->execute();
        if($this->stmt && $this->rowCount() !== 0){
  
          $data = [];
          while($rows = $this->fetch()){$data[] = $rows;}
          return $data;
    
        }else{return [];}
  }

  /**
   * @return count
   */
  public function queryCount($column , $table , $other = null) {
        $sql = "SELECT {$column} FROM `{$table}` {$other}";

        $this->stmt = $this->dbh->query($sql);
        $this->execute();

        if($this->stmt){return $this->rowCount();}
        else{return false;}
  }

  /**
   * Inserting Porcess, preparing the sql format & bind
   */ 
  public function insert($table,$column,array $valueArr){
      //adjust binding keys by using column names
      $binds   = str_replace('`','',$column);  
      $binds   = explode(',',$binds);
      /**
       * FixBug:: Temporaty Fix
       * Last Element has Coma Came from  class Specification
       * Function validateProductType()
       * $this->keys .= "`{$property}`,";
       * */
      if(end($binds) == '' || empty(end($binds)) ){array_pop($binds);} 
      if($column[-1] == ',') {$column = substr($column,0,-1);}

      $bindArray = array_map(function ($item) {
        return ':'.$item.'_';
      }, $binds);
      $bindKeys = implode(' , ',$bindArray);
      
      
      $sql = "INSERT INTO `{$table}` ({$column}) VALUES ({$bindKeys})";   
     
      $this->stmt = $this->dbh->prepare($sql);
      $this->bindParam($bindArray,$valueArr);
  }
  
  public function delete($table, $other) {
      $this->stmt = $this->dbh->prepare( "DELETE FROM `{$table}` WHERE {$other}");
  }

  public function execute(){return $this->stmt->execute();}

  public function rowCount(){return $this->stmt->rowCount();}

  public function fetch(){return $this->stmt->fetch(PDO::FETCH_ASSOC);}
  
  public function lastId(){return $this->dbh->lastInsertId();}
  /**
   * @return bindParam(:key,value)
   */   
  public function bindParam(array $arrKey,array $arrVal){
      
    if(count($arrKey) == count($arrVal)){
      for($i=0; $i <= count($arrKey)-1 ;$i++ ){
        $this->stmt->bindParam($arrKey[$i] , $arrVal[$i]);  
      }
    }

  }

/**
 * @return bindParam(num,value)
 */ 
  public function bindParamQuestions(int $num,array $arrVal){
      
    if($num == count($arrVal)){
      for($i=0; $i < $num ;$i++ ){
       
        $this->stmt->bindParam($i+1 , $arrVal[$i]);  
      }
    }

  }

    
  function __destruct(){
      //  echo "end database"; 
      $this->conn = null;
      $this->dbh = null;
      
  }


}